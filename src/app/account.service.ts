import { Injectable } from '@angular/core';
import { UserAccount } from './data-models/UserAccount';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  public account?: UserAccount;

  constructor() { }

  public toggleSignIn() {
    this.account = this.account ? undefined : { username: 'example' };
  }
}
