import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PensionLookupComponent } from './pension-lookup.component';

describe('PensionLookupComponent', () => {
  let component: PensionLookupComponent;
  let fixture: ComponentFixture<PensionLookupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PensionLookupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PensionLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
