import { Component, OnInit } from '@angular/core';
import { PensionLookupService } from '../pension-lookup.service';

@Component({
  selector: 'app-pension-lookup',
  templateUrl: './pension-lookup.component.html',
  styleUrls: ['./pension-lookup.component.scss']
})
export class PensionLookupComponent implements OnInit {

  constructor(public service: PensionLookupService) { }

  ngOnInit(): void {
  }

}
