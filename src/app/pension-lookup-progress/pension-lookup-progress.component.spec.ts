import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PensionLookupProgressComponent } from './pension-lookup-progress.component';

describe('PensionLookupProgressComponent', () => {
  let component: PensionLookupProgressComponent;
  let fixture: ComponentFixture<PensionLookupProgressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PensionLookupProgressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PensionLookupProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
