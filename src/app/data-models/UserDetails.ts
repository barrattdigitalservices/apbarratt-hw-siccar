export class UserDetails {
  public firstname: string = '';
  public lastname: string = '';
  public gender: string = '';
  public dateOfBirth: Date = new Date();
  public postcode: string = '';
  public nationalInsurance: string = '';
  public consentForUnspecifiedDataUsage: boolean = false; // I wish!
}