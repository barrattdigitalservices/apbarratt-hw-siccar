import { UserDetails } from "./UserDetails";

export interface UserAccount {
  username: string,
  details?: UserDetails,
}