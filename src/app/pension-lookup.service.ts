import { Injectable } from '@angular/core';
import { UserDetails } from './data-models/UserDetails';

@Injectable({
  providedIn: 'root'
})
export class PensionLookupService {
  public userDetails: UserDetails;
  public lookupState: 'unstarted' | 'submitted' | 'complete';

  constructor() {
    this.userDetails = new UserDetails();
    this.lookupState = 'unstarted';
  }

  public lookup = () => {
    this.lookupState = 'submitted';
    setTimeout(() => {
      this.lookupState = 'complete';
    }, 3500);
  }
}
