import { TestBed } from '@angular/core/testing';

import { PensionLookupService } from './pension-lookup.service';

describe('PensionLookupService', () => {
  let service: PensionLookupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PensionLookupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
