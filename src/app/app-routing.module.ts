import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PensionLookupComponent } from './pension-lookup/pension-lookup.component';

const routes: Routes = [
  {path: '', component: PensionLookupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
