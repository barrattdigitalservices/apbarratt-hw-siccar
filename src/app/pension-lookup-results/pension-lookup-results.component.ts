import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pension-lookup-results',
  templateUrl: './pension-lookup-results.component.html',
  styleUrls: ['./pension-lookup-results.component.scss']
})
export class PensionLookupResultsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
