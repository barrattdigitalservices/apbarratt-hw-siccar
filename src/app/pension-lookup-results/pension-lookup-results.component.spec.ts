import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PensionLookupResultsComponent } from './pension-lookup-results.component';

describe('PensionLookupResultsComponent', () => {
  let component: PensionLookupResultsComponent;
  let fixture: ComponentFixture<PensionLookupResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PensionLookupResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PensionLookupResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
