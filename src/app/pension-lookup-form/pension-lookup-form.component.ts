import { Component, OnInit } from '@angular/core';
import { PensionLookupService } from '../pension-lookup.service';

@Component({
  selector: 'app-pension-lookup-form',
  templateUrl: './pension-lookup-form.component.html',
  styleUrls: ['./pension-lookup-form.component.scss']
})
export class PensionLookupFormComponent implements OnInit {

  constructor(public service: PensionLookupService) { }

  ngOnInit(): void {
  }

}
