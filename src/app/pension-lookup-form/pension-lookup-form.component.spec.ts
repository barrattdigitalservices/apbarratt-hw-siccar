import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PensionLookupFormComponent } from './pension-lookup-form.component';

describe('PensionLookupFormComponent', () => {
  let component: PensionLookupFormComponent;
  let fixture: ComponentFixture<PensionLookupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PensionLookupFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PensionLookupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
